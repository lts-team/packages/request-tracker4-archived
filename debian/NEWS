request-tracker4 (4.4.1-1) experimental; urgency=medium

  Below are some specific notes about changes in this major new release
  of RT, but please also review in full the notes at
  /usr/share/doc/request-tracker4/UPGRADING-4.4.gz.

  This version of RT incorporates several plugins, which should be removed
  from the system if installed locally to prevent conflicts:
  - RT::Authen::ExternalAuth
  - RT::Extension::LDAPImport
  - RT::Extension::Assets
  - RT::Extension::SLA
  - RT::Extension::ExternalStorage
  - RT::Extension::ParentTimeWorked
  - RT::Extension::AddAttachmentsFromTransactions
  - RT::Extension::SpawnLinkedTicketInQueue
  - RT::Extension::FutureMailgate

  Please see /usr/share/doc/request-tracker4/UPGRADING-4.4.gz for
  additional steps which might be required when migrating from one
  of these extensions to RT 4.4.

  This version of RT includes a database content upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.13 --upgrade-to 4.4.1

  Note: if you are upgrading from a different version than 4.2.13,
  specify that version number instead.

  Since RT 4.4, configuration snippets in
  /etc/request-tracker4/RT_SiteConfig.d are read by RT itself if named
  with the .pm extension. The post-install script will rename files
  managed by the package accordingly, but other files will need to be
  manually renamed by the administrator. The update-rt-sitconfig script
  is now obsolete and has been removed.

 -- Dominic Hargreaves <dom@earth.li>  Mon, 02 Jan 2017 17:37:08 +0000

request-tracker4 (4.2.11-1) unstable; urgency=medium

  This version of RT includes a database content upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.10 --upgrade-to 4.2.11

  The full-text indexing defaults for PostgreSQL have changed; GiST is now
  the suggested index, as well as storing data in a separate
  AttachmentsIndex table.  Both changes improve lookup speed.  For
  improved search performance, you may wish to drop existing tsvector
  and GIN indexes on Attachments, and re-generate the index using
  rt-setup-fulltext-index.

 -- Dominic Hargreaves <dom@earth.li>  Mon, 04 May 2015 18:23:45 +0100

request-tracker4 (4.2.10-1) experimental; urgency=medium

  This version of RT includes a database content upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.9 --upgrade-to 4.2.10

 -- Dominic Hargreaves <dom@earth.li>  Sun, 29 Mar 2015 20:53:28 +0100

request-tracker4 (4.2.7-1) unstable; urgency=high

  This version of RT includes a database content upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.6 --upgrade-to 4.2.7

 -- Dominic Hargreaves <dom@earth.li>  Fri, 12 Sep 2014 23:08:11 +0100

request-tracker4 (4.2.6-1) unstable; urgency=medium

  This version of RT includes a database content and schema upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.4 --upgrade-to 4.2.6

 -- Dominic Hargreaves <dom@earth.li>  Sat, 06 Sep 2014 20:11:06 +0100

request-tracker4 (4.2.4-1) unstable; urgency=medium

  This version of RT includes a database content upgrade (although it
  is implemented as a 'schema' file, it does not change the database
  schema). If you are using a dbconfig-managed database, you will be
  offered the choice of applying this automatically; if not, please
  apply them separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.2.3 --upgrade-to 4.2.4

 -- Dominic Hargreaves <dom@earth.li>  Sun, 18 May 2014 22:36:53 +0100

request-tracker4 (4.2.3-1) experimental; urgency=medium

  This version of RT includes a database content and schema upgrade.
  If you are using a dbconfig-managed database, you will be offered the
  choice of applying this automatically; if not, please apply them
  separately using something like:

  rt-setup-database-4 --action upgrade --upgrade-from 4.0.19 --upgrade-to 4.2.3

  This version of RT is a major upgrade from the 4.0.x series and includes
  some incompatible changes. Please check
  /usr/share/doc/request-tracker4/UPGRADING-4.2.gz for anything which
  may need action for your installation.

  This version of the RT package changes the logging configuration to
  log to a file rather than syslog by default. Please check that this
  configuration is suitable for your installation.

 -- Dominic Hargreaves <dom@earth.li>  Mon, 24 Feb 2014 22:16:14 +0000

request-tracker4 (4.0.19-1) unstable; urgency=medium

  This version of RT includes a database content and schema upgrade
  (the latter for MySQL only). If you are using a dbconfig-managed database,
  you will be offered the choice of applying this automatically; if not,
  please apply them separately using something like:

  rt-setup-database-4 --action insert --datafile /usr/share/request-tracker4/etc/upgrade/4.0.18/content --skip-create
  rt-setup-database-4 --action insert --datafile /usr/share/request-tracker4/etc/upgrade/4.0.19/content --skip-create

  and in addition, for MySQL:

  rt-setup-database-4 --action schema --datafile /usr/share/request-tracker4/etc/upgrade/4.0.19/schema.mysql --skip-create

 -- Dominic Hargreaves <dom@earth.li>  Sun, 16 Feb 2014 16:15:23 +0000

request-tracker4 (4.0.12-2) unstable; urgency=high

  This version of RT includes a database content upgrade (although it
  is implemented as a 'schema' file, it does not change the database
  schema). If you are using a dbconfig-managed database, you will be
  offered the choice of applying this automatically; if not, please
  apply them separately using something like:

  rt-setup-database-4 --action schema --datafile /usr/share/request-tracker4/etc/upgrade/4.0.13/schema.Pg --skip-create

  or

  rt-setup-database-4 --action schema --datafile /usr/share/request-tracker4/etc/upgrade/4.0.13/schema.mysql --skip-create

  Note that there is no update available for SQLite databases; they are not
  recommended for production use.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 11 May 2013 18:01:12 +0100

request-tracker4 (4.0.12-1) unstable; urgency=low

  This version of RT includes a database content upgrade (although it
  is implemented as a 'schema' file, it does not change the database
  schema). If you are using a dbconfig-managed database, you will be
  offered the choice of applying this automatically; if not, please
  apply them separately using something like:

  rt-setup-database-4 --action schema --datafile /usr/share/request-tracker4/etc/upgrade/4.0.12/schema.Pg --skip-create

  Note that schema.Pg can be used for MySQL, PostgreSQL and SQLite.

 -- Dominic Hargreaves <dom@earth.li>  Sat, 11 May 2013 14:52:32 +0100

request-tracker4 (4.0.10-1) experimental; urgency=low

  This version of RT includes a database content upgrade. If you are using
  a dbconfig-managed database, you will be offered the choice of applying
  this automatically; if not, please apply them separately using
  something like:

  rt-setup-database-4 --action insert --datafile /usr/share/request-tracker4/etc/upgrade/4.0.9/content --skip-create

 -- Dominic Hargreaves <dom@earth.li>  Wed, 27 Mar 2013 22:36:31 +0000

request-tracker4 (4.0.7-5) unstable; urgency=medium

  This release changes the default location of variable data in RT
  from /var/cache/request-tracker4 to /var/lib/request-tracker4; this
  fixes a long-standing bug where non-cache data was put in /var/cache.
  The GPG data are moved automatically to the new location by the postinst,
  but other data, such as Sphinx indexes, are not.
  
  It is recommended that you check /var/cache/request-tracker4 for other
  data which should live elsewhere, move them to /var/lib/request-tracker4,
  and update configuration files (such as the Sphinx configuration
  generated by rt-setup-fulltext-index) manually. However
  /var/cache/request-tracker4/mason_data and
  /var/cache/request-tracker4/session_data should not be moved.

 -- Dominic Hargreaves <dom@earth.li>  Thu, 28 Mar 2013 22:42:02 +0000

request-tracker4 (4.0.6-1) unstable; urgency=low

  The web-based query builder now uses Queue limits to restrict the set of
  displayed statuses and owners.  As part of this change, the %cfqueues
  parameter was renamed to %Queues; if you have local modifications to any
  of the following Mason templates, this feature will not function
  correctly:

      share/html/Elements/SelectOwner
      share/html/Elements/SelectStatus
      share/html/Prefs/Search.html
      share/html/Search/Build.html
      share/html/Search/Elements/BuildFormatString
      share/html/Search/Elements/PickCFs
      share/html/Search/Elements/PickCriteria

  This version of RT introduces some Cross-Site Request Forgery protection
  which may break some existing legitimate uses of RT. If you are affected
  please look at the new configuration variables $RestrictReferrer and
  @ReferrerWhitelist.

  This version of RT includes a database schema upgrade for MySQL and
  a database content update for all database versions. If you are using a
  dbconfig-managed database, you will be offered the choice of applying 
  these automatically; if not, please apply them separately
  using something like:

  rt-setup-database-4 --action schema --datafile /usr/share/request-tracker4/etc/upgrade/4.0.6/schema.mysql --skip-create
  rt-setup-database-4 --action insert --datafile /usr/share/request-tracker4/etc/upgrade/4.0.6/content --skip-create

 -- Dominic Hargreaves <dom@earth.li>  Sun, 27 May 2012 17:28:19 +0100

request-tracker4 (4.0.1~rc1-1) experimental; urgency=low

  This new version of RT includes RTFM, which was previously packaged
  as an extension.

  This version of the RT4 package, does not include any automated database
  upgrade/migration from RT3.8. This may appear in the future. In the
  meantime, if you are planning to migrate an existing RT3.8 or earlier
  database, please see README.Debian for some advice.
  
 -- Dominic Hargreaves <dom@earth.li>  Thu, 14 Apr 2011 19:23:18 +0100
